package com.app.resolviendo.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.resolviendo.R;
import com.app.resolviendo.Volley.ApiCall;
import com.app.resolviendo.Volley.VolleyCallback;
import com.app.resolviendo.adapters.SubCategoriesAdapter;
import com.app.resolviendo.helpers.SpacesItemDecoration;
import com.app.resolviendo.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SubCategoriesActivity extends AppCompatActivity {

    private static final int NUM_COLUMNS = 2;
    JSONArray subcategoriesArray = new JSONArray();
    private RecyclerView subCategories;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categories);
        subCategories = (RecyclerView) findViewById(R.id.subCategories);
        Intent intent = getIntent();
        try {
            subcategoriesArray = new JSONArray(intent.getStringExtra("subcategoriesArray"));
            id = getIntent().getStringExtra("subCategoryId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }


        try {
            getSubCategory();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ImageView backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getSubCategory() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);

        ApiCall.PostMethodHeaders(this, UrlHelper.LIST_SUB_CATEGORY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                subcategoriesArray = response.optJSONArray("list_subcategory");
                subCategories.addItemDecoration(new SpacesItemDecoration(14));
                subCategories.setLayoutManager(new GridLayoutManager(SubCategoriesActivity.this, NUM_COLUMNS));
                SubCategoriesAdapter adapter = new SubCategoriesAdapter(SubCategoriesActivity.this, subcategoriesArray);
                subCategories.setAdapter(adapter);

            }
        });

    }
}
