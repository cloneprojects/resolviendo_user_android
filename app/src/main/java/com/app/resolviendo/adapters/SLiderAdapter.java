package com.app.resolviendo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.resolviendo.R;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by karthik on 06/10/17.
 */

public class SLiderAdapter extends RecyclerView.Adapter<SLiderAdapter.MyViewHolder> {
    private Context context;
    private JSONArray categories;

    public SLiderAdapter(Context context, JSONArray categories) {
        this.context = context;
        this.categories = categories;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView categoryName;
        ImageView categoryImage;

        MyViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.categoryName);
            categoryImage = view.findViewById(R.id.categoryImage);
        }
    }

    @Override
    public SLiderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_slider, parent, false);

        return new SLiderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SLiderAdapter.MyViewHolder holder, int position) {

        JSONObject jsonObject = categories.optJSONObject(position);
        Glide.with(context).load(jsonObject.optString("banner_logo")).into(holder.categoryImage);
        holder.categoryName.setText(jsonObject.optString("banner_name"));


    }

    @Override
    public int getItemCount() {
        return categories.length();
//        return 5;
    }
}


